#include <conio.h>
#include <iostream>
#include <string>

using namespace std;

enum DoorState
{
	OPEN, CLOSING, CLOSED, OPENING
};

struct Door
{
	DoorState state;
	bool islocked;
};


struct Student 
{
	string firstName;
	string lastName;
	float gpa;
};

int main() 
{
	//instantiate a new struct
	Student s1;
	s1.firstName = "Brian";
	s1.lastName = "Foote";
	s1.gpa = 1.7f;

	_getch();
	return 0;
}